import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import ubuntu from '@/assets/fonts/Ubuntu.ttf';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: true,
        font: ubuntu,
    },
});
